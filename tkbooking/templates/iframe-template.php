<?php get_header() ?>
<script>
  window.addEventListener('message', function(event) { 
    //  Check the origin of the data! 
    if (~event.origin.indexOf('https://cofer.travel')) { 
        // The data has been sent from your site 
        if (event.data.startsWith('Height:')){
           var height = parseInt(event.data.split(':')[1]);
           
           var elementIframe = document.getElementById('tkbooking_iframe');
           var computed = window.getComputedStyle(elementIframe, null);
           if (computed) {
             height = height + parseInt(computed.getPropertyValue('padding-top')) + parseInt(computed.getPropertyValue('padding-bottom'));
           }
          
           console.log(height)
           elementIframe.style.height = height + 'px';
        }
    } else {  
        return; 
    }
});

</script>

<div class="wrap">
    <iframe
      id="tkbooking_iframe"
      class="container"
      src="<?php echo $tkbooking_iframe_src; ?>"
      frameborder="0"
      width="<?php echo $tkbooking_iframe_width; ?>"
      height="<?php echo $tkbooking_iframe_height; ?>"
      scrolling="no"
      style="overflow: hidden; <?php echo $tkbooking_iframe_style; ?>"
      onload="this.contentWindow.postMessage('tkbooking-background-white', '*')"></iframe>
</div><!-- .wrap -->

<?php get_footer() ?>
