<?php
/**
 * @package Tk_Booking
 * @version 0.1
 */
/*
Plugin Name: Tk Booking Widget
Plugin URI: https://bitbucket.org/plusplusnt/tkbooking-wp-plugin/
Description: This plugin is easy way to add TK Booking Widget to your site.
Author: Aleksandar Milosevic
Version: 0.1
Author URI: http://amilosevic.github.io//
*/

register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );
register_activation_hook( __FILE__, 'tkbooking_activation' );

function tkbooking_activation() {
    tkbooking_add_rewrite_rules();
    flush_rewrite_rules();
}

function tkbooking_deactivation() {
    flush_rewrite_rules();
}

function tkbooking_add_rewrite_rules() {
    add_rewrite_rule('^partner/booking/go','index.php?tk=go','top');
    add_rewrite_rule('^partner/booking/email/([a-z0-9-]+)/confirm','index.php?tk=email&tk_uuid=$matches[1]','top');
    add_rewrite_rule('^partner/booking/accept/([a-z0-9-]+)','index.php?tk=accept&tk_uuid=$matches[1]','top');
    add_rewrite_rule('^partner/booking/status/([a-z0-9-]+)','index.php?tk=status&tk_uuid=$matches[1]','top');
}

add_action('init', 'tkbooking_add_rewrite_rules');

add_filter( 'query_vars', function( $query_vars ) {
    $query_vars[] = 'tk';
    $query_vars[] = 'tk_uuid';
    return $query_vars;
});

add_action( 'template_include', function($template) {    
    $tk = get_query_var('tk');
    
    if ( $tk == 'go' ) {
      return plugin_dir_path( __FILE__ ) . 'templates/go.php';
    } else if ( $tk == 'email' ) {
      return plugin_dir_path( __FILE__ ) . 'templates/email.php';
    } else if ( $tk == 'accept') {
      return plugin_dir_path( __FILE__ ) . 'templates/accept.php';
    } else if ( $tk == 'status') {
      return plugin_dir_path( __FILE__ ) . 'templates/status.php';
    }
    
    return $template;
});

// --- Admin ---
add_action( 'admin_menu', 'tkbooking_admin_menu' );
function tkbooking_admin_menu() {
    add_options_page( 'TK Booking Plugin', 'TK Booking Widget', 'manage_options', 'tkbooking-plugin', 'tkbooking_options_page' );
}

add_action( 'admin_init', 'tkbooking_admin_init' );
function tkbooking_admin_init() {
    register_setting( 'tkbooking-iframe-settings', 'tkbooking-iframe-height' );
    register_setting( 'tkbooking-iframe-settings', 'tkbooking-iframe-width' );
    register_setting( 'tkbooking-iframe-settings', 'tkbooking-iframe-style', array('type' => 'string', 'default' => 'padding-top:50px; display: block;') );
    add_settings_section( 'tkbooking-iframe-settings', 'IFrame settings', 'tkbooking_iframe_section_callback', 'tkbooking-plugin' );
    add_settings_field( 'tkbooking-iframe-height', 'Iframe height', 'tkbooking_iframe_height_callback', 'tkbooking-plugin', 'tkbooking-iframe-settings' );
    add_settings_field( 'tkbooking-iframe-width', 'Iframe width', 'tkbooking_iframe_width_callback', 'tkbooking-plugin', 'tkbooking-iframe-settings' );
    add_settings_field( 'tkbooking-iframe-style', 'Iframe inline css style', 'tkbooking_iframe_style_callback', 'tkbooking-plugin', 'tkbooking-iframe-settings' );
}

function tkbooking_iframe_section_callback() {
    echo 'Height is adjusted to size of iframe document. Height value is used only if communication with iframe failed somehow.';
}

function tkbooking_iframe_height_callback() {
    $setting = esc_attr( get_option( 'tkbooking-iframe-height' ) );
    echo "<input type='text' name='tkbooking-iframe-height' value='$setting' />";
}

function tkbooking_iframe_width_callback() {
    $setting = esc_attr( get_option( 'tkbooking-iframe-width' ) );
    echo "<input type='text' name='tkbooking-iframe-width' value='$setting' />";
}

function tkbooking_iframe_style_callback() {
    $setting = esc_attr( get_option( 'tkbooking-iframe-style' ) );
    echo "<input type='text' name='tkbooking-iframe-style' value='$setting' size='70' />";
}


function tkbooking_options_page() {
    ?>
    <div class="wrap">
        <h2>TK Booking Widget Plugin Options</h2>
        <form action="options.php" method="POST">
            <?php settings_fields( 'tkbooking-iframe-settings' ); ?>
            <?php do_settings_sections( 'tkbooking-plugin' ); ?>
            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}