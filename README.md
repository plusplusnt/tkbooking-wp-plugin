# WP Plugin: tkbooking-wp-plugin

Ovo je plugin koji olakšava integraciju tk/cofer.travel **<iframe\>** booking widget-a na WP sajtovima. 

## Uputstvo za upotrebu

 1. U sekciji [Downloads](https://bitbucket.org/plusplusnt/tkbooking-wp-plugin/downloads/) preuzeti poslednju verziju plugina
 2. Uploadovati plugin u WordPress instancu i aktivirati ga
 3. Posle aktiviranja plugina potrebno je napraviti WordPress stranice koje sadrže tabele cenovnika sa ispravnim linkovima za booking.
 
Pogledati [ovaj .pdf dokument](https://bitbucket.org/plusplusnt/tkbooking-wp-plugin/downloads/tk-Bookingwidget-221018-1301-2.pdf) za detalje:

 * kako najlakše napraviti cenovnik i izvući podatke iz booking backoffice softvera.
 * kako funkcioniše booking preko cofer sistema

Ovaj plugin obezbeđuje gotove iframeove, pravi WordPress rewrite za naznačene url-ove u sekciji **"Sta je jos potrebno uraditi"** (strana 5 pdf dokumenta).
	
## Test instanca

Postoji test instanca na kojoj se može videti kako widget radi i pogledati admin sekcija. 

http://matilda.plusplus.rs:9080


